package com.example.demo;

import org.springframework.data.rest.core.config.Projection;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Projection(name = "user", types = { User.class })
public interface UserProjection {

	String getName();

	String getAddress();

	@JsonIgnoreProperties("address")
	UserProjection getManager();

	@JsonIgnoreProperties("address")
	UserProjection getAnotherManager();
}
