/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DatabaseLoader implements CommandLineRunner {

	private final UserRepository userRepository;

	@Autowired
	public DatabaseLoader(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public void run(String... strings) throws Exception {
		User manager = new User(null, "manager", "ADDRESS THAT SHOULD NOT BE SHOWN");
		userRepository.save(manager);
		User anotherManager = new User(null, "eager manager", "ANOTHER ADDRESS THAT SHOULD NOT BE SHOWN");
		userRepository.save(anotherManager);
		User user = new User(null, "user", "address");
		user.setManager(manager);
		user.setAnotherManager(anotherManager);
		user = userRepository.save(user);
	}
}
